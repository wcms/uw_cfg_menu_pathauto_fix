<?php

/**
 * @file
 * uw_cfg_menu_pathauto_fix.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_cfg_menu_pathauto_fix_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_pathauto_fix_content_types';
  $strongarm->value = array(
    'uwaterloo_custom_listing' => 'uwaterloo_custom_listing',
    'uw_image_gallery' => 'uw_image_gallery',
    'uw_web_form' => 'uw_web_form',
    'uw_web_page' => 'uw_web_page',
  );
  $export['menu_pathauto_fix_content_types'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_pathauto_fix_debug';
  $strongarm->value = '0';
  $export['menu_pathauto_fix_debug'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_pathauto_fix_menus';
  $strongarm->value = array(
    'menu-audience-menu' => 'menu-audience-menu',
    'main-menu' => 'main-menu',
  );
  $export['menu_pathauto_fix_menus'] = $strongarm;

  return $export;
}
